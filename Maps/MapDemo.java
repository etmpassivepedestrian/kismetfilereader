import java.util.*;

public class MapDemo{
    //public static Map<String, String> collection = new HashMap<String, String>();
    public static Map<String, Integer> collection = new HashMap<String, Integer>(); 
    public static void main(String[] args){
        for(String a : args){
            Integer freq = collection.get(a.toLowerCase());
            collection.put(a, ((freq == null) ? 1 : freq + 1));
        }
        
        System.out.printf("%d distinct words \n", collection.size());
        System.out.println(collection);
        // toString() will print the collection, similar to:
        // {to=3, delegate=1, be=1, it=2, up=1, if=1, me=1, is=2}
    }
}

// Map API Quick Reference

// Map<K,V>

// int size() 
//     Returns the number of Key-Value instances
// V get()
// V put()
// V remove(Object key)
//     Returns V as an indicator that it has removed
//     mapping for 'Object key' else returns null.
