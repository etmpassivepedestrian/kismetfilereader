import java.util.*;
import java.io.*;

public class Interp
{
   public static void main(String[] args) throws IOException
   {
	   if (args.length == 2 || args.length == 3)
	   {
		   //check if we want to save in increments of 1, or by filename
		   boolean outputByIncrements = false;
		   int fileIncrement = 0;
		   if (args.length == 3)
		   {
			   if (args[2].toLowerCase().equals("true"))
			   {
				   outputByIncrements = true;
			   }
		   }
		   
		   //We set up the start of our tsv file with a header
		   String output = "Node\tTime\tDetections\n";
		   
		   //Take the first argument, which is the directory we are reading from
		   File directory = new File(args[0]);
		   
		   String dirs = args[0].split("/")[args[0].split("/").length - 1];
		   
		   //Create an array of all of the files in the directory
		   File[] directoryFiles = directory.listFiles();
		   if (directoryFiles != null)
		   {
			   for (File childFile : directoryFiles)
			   {
				   //So long as the file has the .log extension, we will do work on it.
				   if (childFile.getName().matches("(.*).log"))
				   {
					   Scanner reader = new Scanner(childFile);
					   HashSet<String> detectSet = new HashSet<String>();
					   
					   while (reader.hasNextLine())
					   {
						   String[] line = reader.nextLine().split("\\s");
						   
						   //If there is a valid entry, we add it to our set of detections
						   if (line[0].equals("*CLIENT:") && !line[2].equals("1"))
						   {
							   detectSet.add(line[1]);
						   }
					   }
					   //Output each line formatted for tab seperated values
					   if (outputByIncrements)
					   {
						   output += dirs + "\t" + fileIncrement + "\t" + detectSet.size() + "\n";
						   fileIncrement += 1;
					   }
					   else
					   {
						output += dirs + "\t" + childFile.getName().replace(".log", "") + "\t" + detectSet.size() + "\n";			
					   }
				   }
			   }
			   //write to our output destination
			   File outFile = new File(args[1]);
			   BufferedWriter fileWrite = new BufferedWriter(new FileWriter(outFile, false));
			   fileWrite.write(output);
			   fileWrite.close();
		   }
	   }
	   //if incorrect arguments length, tell the user how to use the program
	   else
	   {
		   System.out.println("Usage: java Interp %DIRECTORYPATH% %OUTPUTFILE%");
	   }
   }
}