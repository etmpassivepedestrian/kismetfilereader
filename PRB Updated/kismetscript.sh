#Kismet script v0.3
#Joshua Bond

#Set date and time for script
CURRENTDATE=$(date +"%Y-%m-%d-%H-%M-%S")

echo $CURRENTDATE

#stop any existing runs of the kismet server, which allows the previous execution of the script to run the rest of the commands
echo "KILL any existing kismet servers"
killall kismet_server

#reduced this time from 10 to 5, as catchup was too slow when running multiple scripts
echo "Executing kismet script"
echo "Sleep for 5 seconds before running kismet, this ensures that wlan1 is available"
sleep 5

#This ensures that the kismet server runs in the background, allowing the netcat command to run immediately afterwards
#This also provides a handy log of the output of the kismet server to nohup.out in the home directory
sudo -u root nohup kismet_server &
echo "Sleep 3 before running netcat, to ensure the server is up"
sleep 3

#create empty file using current date, avoid using ':' in filename as SSH will interpret this as an address
#without the touch command we cannot pipe our logs into the file, as it will complain that it does not exist
touch /root/kismetlogs/$CURRENTDATE.log

#This command must be encapsulated in a single line. Without creating the NETCOMMAND variable, this will not work.
#You cannot simply run the contents of the command as the '|' will be interpreted as a command on a new line.
echo "Piping netcat into logfile /root/kismetlogs/$CURRENTDATE.log"
NETCOMMAND=$(sudo -u root echo -e '!0 enable client MAC,type' | nc localhost 2501 >> /root/kismetlogs/$CURRENTDATE.log)

$NETCOMMAND

#NETCOMMAND will run until something kills the kismet server, in this case, a new instance of our script, once the server dies
#this script will get past the NETCOMMAND above, and tell us (if we are running the script manually). This can also be placed
#in a log file if needed, by cat-ing the results of the script, similar to the netcat logs.
echo "Netcat terminated"

#change to local log directory, this is not longer necessary when using ssh, as it supports full paths, but lets use it anyway
cd /root/kismetlogs

#sleep here to allow the newer script that is currently running to 'catch' up, this allows the pi to avoid hogging too much power
#and killing the network when this script attempts to send to the server. the significant length of time here has no impact
#as the logs have already been finalized by this point.
echo "Sleeping 20 to allow new instance of script to catch up"
sleep 20

#depricated, was used for FTP transfers
#chmod 777 "$CURRENTDATE.log"


#Restart network service to ensure connectivity upon send, without this command, if the pi does not have enough power from the usb
#it will remain connected to the network, but reject all attempts to use it. If a sufficient power supply is used, this should not be needed
#Conveniently because kismet has already taken control of wlan0, and network manager has no control of it, it does not interrupt our logs
echo "Restarting network interface to ensure connection"
sudo -u root service network-manager restart

#sleep for 10 seconds to allow the network interface to reconnect to wireless before sending logs
sleep 10


#send over ssh to ltnodeserver.no-ip.org
#sshpass is used to skip the confirmation dialogue when scp asks for the password to the server
#ssh key should be added to root/.ssh/known_hosts first, if a server with an unknown key is added, this will not work
echo "Sending log to server"
sshpass -p nodepass scp "$CURRENTDATE.log" "nodeserver@ltnodeserver.no-ip.org:logs/$CURRENTDATE.log"



#depricated send commands

#FTP over ngrok address, this ultimately caused too many problems with file permissions and network connectivity.
#Has since been replaced with SSH

#wput -p "$CURRENTDATE.log" ftp://nodeserver:nodepass@0.tcp.ngrok.io:12585/
#wput "2016-07-17:06:30:14.log" ftp://ftpuser:grootftp@192.168.0.25:21/
#      file name here                 username:password@address of ftp server running vsftpd :21 (portnumber)
