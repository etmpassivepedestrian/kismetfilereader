import java.util.*;
import java.io.*;
//import java.
//import java.
/**
 * Class to read and Analyze Kismet OutputFiles
 * @author Daniel Minero, Randal Liang
 *
 */
public class KismetFileReader
{
	/**
	 *  ONLY Use the keyboard for any user input if need
	 */
	private Scanner keyboard;
	private Scanner input;
	/*
	 *  Statistics Variables Below
	 *  TODO: Insert here any variables to stores data
	 */
	private int pingCount;
    private int totalPackets;
	private int startTime;
	private int endTime;
	private int count;
    private Map<String, Integer> macAddresses;
	private ArrayList<Integer> list;
    //Use size() to obtain the number of unique devices
    /**
     * Constructor for reading a Kismet files
     * @param filename - name of the text file (with extension)
     * @throws FileNotFoundException cannot find the file
     */
    public KismetFileReader(String filename) throws FileNotFoundException
	{
        this.input = new Scanner(new File(filename));
        this.pingCount = 0;
        this.totalPackets = 0;
        this.startTime = 0;
        this.endTime = 0;
		this.count = 0;
        this.macAddresses = new HashMap<String, Integer>();
		this.list = new ArrayList<Integer>();
    }
	/**
	 * This method will read the given input file from the constructor
	 * 
	 */
	public void read()
	{
		//Notes:
		/* 1. File is always in the same format
		 * 2. Divide into sections by header words like CLIENT, TIME
		 * Design:
		 * 1. Assumes that file was given in the constructor
		*/
		
        this.keyboard = new Scanner(System.in);
		String[] words;
		String line;
        boolean flag = false;
		
		//TODO: Read Overhead Data (if needed)
		line = input.nextLine();
		words = line.split("[^(A-F|\\d)(A-F|\\d)\\:?\\w\\.\\,\\*]+");
		
		//TODO: Read Protocols Data (if needed)
		line = input.nextLine();
		words = line.split("[^(A-F|\\d)(A-F|\\d)\\:?\\w\\.\\,\\*]+");
		
		//TODO: Read Initial Address (What are these again?)
		while (input.hasNext())
		{
			line = input.nextLine();
			if(line.contains("ACK"))
			{
				break;
			}
		}
		
		//Read Packet Data
		while (input.hasNext())
		{
			line = input.nextLine();        
			//http://regexr.com/
			/*
			 * REGEX EXPLAINED
             * (A-F|\\d)(A-F|\\d)\\:?   -> this alone finds all MAC addresses in the file
             * \\w\\.\\,\\*             -> \\w collects words, \\, includes the comma, includes the asterisk
             * [^AB]                    -> assume A and B are the groups above, then this code says everything
             *                              but those classes (are the delimiters in the case)
             * [^AB]+                   -> the plus joins any more therafter that match that criteria
			 */
			words = line.trim().split("[^(A-F|\\d)(A-F|\\d)\\:?\\w\\.\\,\\*]+");
			
			for(int i = 0; i < words.length; ++i)
			{
                // if(words.length > 1)
                //     System.out.print("| " + w + " |");
                
                //This works but can be done better
                if(words[i].contains("CLIENT"))
				{
					if(words[i + 2].equals("2"))
					{
						//String address = macAddresses.get(words[i]);
						//if (address == null){
						if (macAddresses.get(words[i + 1]) == null)
						{
							macAddresses.put(words[i + 1], 1);
						}
						else
						{
							Integer freq = macAddresses.get(words[i + 1]);
							macAddresses.put(words[i + 1], freq + 1);
						}
						//(macAddresses.get(words[i]) == null ? macAddresses.put(words[i], 1) : macAddresses.put(words[i], macAddresses.get(words[i]) + 1))
						++totalPackets;
						//TODO: Pass MAC address into collection
						break;
					}
                }
                else if(words[i].contains("TIME"))
				{
                    ++pingCount;
                    //you could do Strings for the int then parseInt() on display
                    if (this.startTime == 0)
					{
                    	startTime = Integer.parseInt(words[i + 1]);
                    }
					else if((this.endTime - this.startTime) >= 300)
					{
						//this.count++;
						//System.out.println("Writing to Output_" + count);
						//writeStatisticsToFile();
						this.list.add(macAddresses.size());
						
						this.pingCount = 0;
						this.totalPackets = 0;
						this.startTime = 0;
						this.endTime = 0;
						this.macAddresses.clear();
					}
                    else
					{
                    	endTime = Integer.parseInt(words[i + 1]);
                    }
                    break;
                }
            }            
            // String cmd = keyboard.nextLine();
            // if(!flag && cmd.equalsIgnoreCase("kill"))
            //     break;
            // else if(!flag && cmd.equalsIgnoreCase("End"))
            //     flag = true;              
		}
        input.close();
        keyboard.close();
	}
	/**
	 * The method that is used to display the statistics to the user
	 * gather from the file read;
	 */
    public void display()
	{
        System.out.println("| -- Statistics -- |");
        System.out.printf("| Log session start: %1$s \n", new Date((long)startTime * 1000).toString() );
        System.out.printf("| Log session end: %1$s \n", new Date((long)endTime * 1000).toString() );        
        System.out.printf("| Total Pings: %d \n", pingCount);
        System.out.printf("| Packets Detected (Non-Unique): %d \n", totalPackets);
        System.out.printf("| Start Time: %d \n", startTime);
        System.out.printf("| End Time: %d \n", endTime);
        System.out.printf("| Duration: %d s \n", endTime - startTime);
        // TODO: Format the output of the map to be more clearer - DONE
        printMACAddresses();
        System.out.println();
        System.out.println("| -- ---------- -- |");
        //TODO: Add more data to collect. Update the member class variables. 
    }
    /**
     * Specific printing function to properly output the contents of MACAddresses
     *
     *
     */
    public void printMACAddresses()
	{
        System.out.println("| List of Devices Detected: \n| ");
        //System.out.println("|\t (Address, Pings)");  
        for (Map.Entry mac : macAddresses.entrySet())
		{
            //System.out.printf("| ( Address: %s Pings: %d ) \n|", mac.getKey(), mac.getValue());
            System.out.printf("|\t Address: %s Pings: %d \n", mac.getKey(), mac.getValue());            
        }
    }
    /**
     * If you need to write the statistics onto a file
     * using a FileWriter??
     * @param output
     */
	public void writeStatisticsToFile()
	{
		try
		{
			PrintWriter writer = new PrintWriter("output_" + count);
			
			writer.println("| -- Statistics -- |");
			writer.printf("| Log session start: %1$s \n", new Date((long)startTime * 1000).toString() );
			writer.printf("| Log session end: %1$s \n", new Date((long)endTime * 1000).toString() );        
			writer.printf("| Total Pings: %d \n", pingCount);
			writer.printf("| Packets Detected (Non-Unique): %d \n", totalPackets);
			writer.printf("| Start Time: %d \n", startTime);
			writer.printf("| End Time: %d \n", endTime);
			writer.printf("| Duration: %d s \n", endTime - startTime);
			writer.printf("| Num of Devices: %d \n", macAddresses.size());
			
			writer.println("| List of Devices Detected: \n| ");
			
			for (Map.Entry mac : macAddresses.entrySet())
			{
				writer.printf("|\t Address: %s Pings: %d \n", mac.getKey(), mac.getValue());            
			}
			
			writer.println();
			writer.println("| -- ---------- -- |");
			
			writer.close();
        }
		catch (Exception e)
		{
			System.out.println("File Not Found!");
            e.printStackTrace();
		}
    }
	
	/**
	 *Writing the stats to file
	 */
	public void writeToFile()
	{
		try
		{
			PrintWriter writer = new PrintWriter("output");
			
			for (int object: list)
			{
				this.count += 5;
				writer.println(count + ";" + object);
			}
			
			writer.close();
        }
		catch (Exception e)
		{
			System.out.println("File Not Found!");
            e.printStackTrace();
		}
	}
	
	/**
	 * Tester main program, remember to specify the file
	 * @param args - the file to be analyzed
	 */
	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.out.println("Cannot run. Usage: KismetFileReader <input file>");
		}
		else
		{
            try
			{
                KismetFileReader kfr = new KismetFileReader(args[0]);
                kfr.read();
                //kfr.display();
				kfr.writeToFile();
            }
            //TODO: Edit Exception Handling
            catch (Exception e)
			{
                System.out.println("File Not Found!");
                e.printStackTrace();
            }
		}
		
	}
}